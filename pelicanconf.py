#########
# Basic #
#########

AUTHOR = 'Yu-Jie Lin'
USE_FOLDER_AS_CATEGORY = False
DEFAULT_CATEGORY = 'assorted'
STATIC_PATHS = [
  'static/UNLICENSE',
  'static/robots.txt',
]
EXTRA_PATH_METADATA = {
  'static/UNLICENSE': {'path': 'UNLICENSE.txt'},
  'static/robots.txt': {'path': 'robots.txt'},
}
IGNORE_FILES = ['.*.swp']
PATH = 'content'
SITENAME = 'Apricot'
SITEURL = 'http://pelican-apricot.bitbucket.org'
TIMEZONE = 'UTC'

CACHE_CONTENT = True
LOAD_CONTENT_CACHE = True

#######
# URL #
#######

YEAR_ARCHIVE_SAVE_AS = '{date:%Y}.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}-{date:%m}.html'

##############
# Pagination #
##############

DEFAULT_PAGINATION = 10

################
# Translations #
################

DEFAULT_LANG = 'en'

##########
# Themes #
##########

THEME = './apricot'
THEME_STATIC_DIR = ''

###########
# Plugins #
###########

PLUGIN_PATHS = ['plugins']
PLUGINS = ['rst-kbd']

#################
# Apricot theme #
#################

SITE_DESCRIPTION = 'simple theme for Pelican'
CSS_INLINE = True

ATTRIBUTION = 4
COPYRIGHT = (
  'Unless otherwise stated, '
  'all contents have been placed in the public domain,<br/>'
  'or via %s, if not applicable.' % (
    "<a rel='license' href='/UNLICENSE.txt'><tt>UNLICENSE</tt></a>"
  )
)
