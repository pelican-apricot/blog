===================
Apricot blog soruce
===================

This repository stores blog posting sources for Pelican Apricot_ theme, you can
read the blog_.

.. _Apricot: https://bitbucket.org/pelican-apricot/theme
.. _blog: http://pelican-apricot.bitbucket.org


Copyright
=========

The contents in this repository have been placed in the public domain, or via
UNLICENSE_, if not applicable.

.. _UNLICENSE: UNLICENSE
