:title: Have My Apricot!
:date: 2016-02-24T06:16:55Z
:author: Yu-Jie Lin

Apricot_ is the theme which I created for my own personal blog,
`Pointless Blog of Apricot`_. Although there are already tons of user-made
Pelican themes in the official repository, I just couldn't find one is simple
and yet still keep some functionality that I would like to have.

.. _Apricot: https://bitbucket.org/pelican-apricot/theme
.. _Pointless Blog of Apricot: https://pba.yjl.im

Therefore, creating my own was the only way to go and I could also learn more
about Pelican. It took me more than a week to have most of things completed,
but there is still many bits I have yet to finish or tweak.

Unlike my blog, this one is hosted by Bitbucket, which I had never used before,
a good opportunity to write a blog and learn a bit of Bitbucket's static
website hosting.
