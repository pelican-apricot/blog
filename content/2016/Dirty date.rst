:title: Dirty date
:date: 2016-02-24T23:05:57Z
:tags: breadcrumbs, Python, date, period archives

While I was coding the template for breadcrumbs, an issue with
``period_archives.html`` arose, the period archive pages.

Because the ``period`` is a tuple and ``YEAR_ARCHIVE_SAVE_AS`` requires a
``datetime`` object, there is no obviously way to do it since you can't have
an access to Python ``datetime`` module without helps.

*I gave up* and skipped breadcrumbs for period archive pages until I realized
that we have as many ``datetime`` objectis as how many articles we have.

And the following piece of code was devilishly coded:

.. code:: jinja

  {# dirty, dirty, dirty date #}
  {% if articles %}
    {% set d = (period + ('January', '1')[period|length - 1:])|join(' ') %}
    {% set d = articles[0].date.strptime(d, '%Y %B %d') %}
  {% endif %}

If assigns missing date components as January and/or 1, then parses the new
date string with ``datetime`` object from the ``date`` of first article using
``strptime`` method. Now we have the ``datetime`` object for the
``*_ARCHIVE_SAVE_AS`` to format the links.

It's *dirty*.
