:title: Test style
:date: 2016-02-28T02:26:20Z

This is a *post* for **testing** |styles|. It's written in **re**\ ``Structured``\ *Text*.

.. |styles| raw:: html

  <strong><em>styles</em></strong>

.. contents:: **Tests**


==========
Paragraphs
==========

One paragraph
=============

**Lorem ipsum** dolor sit amet, consectetur ``adipiscing`` elit. Morbi pellentesque ultricies feugiat. Nunc sit amet dolor ut felis semper pretium. Pellentesque pulvinar metus in justo cursus quis eleifend lectus aliquet.

Paragraphs
==========

In luctus pretium nisl, a commodo risus hendrerit :sup:`in`. Praesent tincidunt interdum nisl :sub:`sit` amet ornare. Duis adipiscing sodales tincidunt. Aliquam sit amet ante arcu. Praesent ante tellus, lobortis non.

Fusce tempor dolor sed nibh pulvinar in facilisis elit pretium. Nulla et eleifend sapien. :t:`Maecenas` a condimentum ante. Nam ut magna ut ipsum pretium consectetur aliquet id felis. Vivamus fringilla.

``<br/>``
=========

| Donec ultrices neque ut nunc facilisis in euismod lacus venenatis. Integer eget orci tortor. Curabitur non turpis sapien. Vivamus interdum.
| Sed eu massa et diam pharetra condimentum in eu nulla. In varius mollis quam, nec cursus arcu rutrum ut. Nulla.

``<kbd/>``
==========

Three-finger salut :kbd:`Ctrl` + :kbd:`Alt` + :kbd:`Del`.

=================
``<blockquote/>``
=================

One quote
=========

  Suspendisse potenti. Vivamus vulputate malesuada dignissim. Praesent tincidunt mauris at ante auctor vitae gravida eros cursus. Nullam sit amet odio elit, a interdum mauris. Curabitur orci dolor, volutpat ullamcorper dignissim.

Two quotes
==========

  Aenean et diam tempor turpis mollis ultrices. Pellentesque adipiscing, nisl a rutrum elementum, nibh lectus varius urna, at venenatis mi.

  Donec posuere, metus nec mattis auctor, lorem metus aliquam nisi, egestas fringilla metus metus ut ligula. Donec erat libero, imperdiet.

Nested
======

  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse commodo venenatis velit, id iaculis justo rutrum viverra. Pellentesque lobortis consectetur.

    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris aliquam quam sit amet odio ornare.

  Proin nisi massa, mollis ac porttitor a, sollicitudin eget eros. Donec scelerisque, enim vel interdum facilisis, mi est aliquet enim.

  .. code:: bash

    printf -v sep '%*s' 58
    echo "${sep// /-}"

  Nam et diam pulvinar nisl molestie auctor vitae at nulla. Curabitur volutpat est eget nulla posuere sit amet consectetur mauris.

  * Pellentesque in sapien tempor urna varius fermentum.
  * Donec dictum euismod nulla, non vulputate sapien adipiscing imperdiet.

  Donec non dolor non leo mattis fringilla.


==========
``<pre/>``
==========

One literal block
=================

::

  Curabitur hendrerit augue id risus imperdiet ultrices tempus vel dolor. Donec sodales, eros ac aliquam ullamcorper, dui turpis pharetra velit.

Two literal blocks
==================

::

  Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus eget leo lorem. Praesent tempor purus.

::

  Aliquam erat volutpat. Nullam vulputate, nunc non tincidunt ultricies, arcu odio sagittis metus, quis consectetur dolor urna et sapien. Morbi.

Surrounded
==========

Nulla facilisi. Sed ut neque magna. Nullam sed enim ac urna euismod porta. In pellentesque tempus tristique. Curabitur semper erat.

::

  Morbi sed gravida lacus. Duis facilisis suscipit mauris rutrum luctus. Curabitur in est libero. Proin eget erat mauris, at hendrerit.

Cras adipiscing tellus non tortor sollicitudin adipiscing. Mauris ut leo ac urna tempus tincidunt sit amet eu justo. Cras risus.


===================
Syntax highlighting
===================

One block
=========

.. code:: bash

  find -type f -printf '%s %p\n' |
  sort --numeric-sort |
  tail |
  while read size path; do
    echo $((size/1024/1024))MB $path
  done

Two blocks
==========

.. code:: bash

  find -type f -printf '%s %p\n' |
  sort --numeric-sort |
  tail |
  while read size path; do
    echo $((size/1024/1024))MB $path
  done

.. code:: bash

  printf -v sep '%*s' 58
  echo "${sep// /-}"

Surrounded
==========

Nullam malesuada purus eget nulla tempus accumsan. Vestibulum et blandit ligula. Sed congue euismod magna vitae semper. Aliquam interdum aliquam.

.. code:: bash

  find -type f -printf '%s %p\n' |
  sort --numeric-sort |
  tail |
  while read size path; do
    echo $((size/1024/1024))MB $path
  done

::

  0MB ./Search.rst
  0MB ./YouTube About.rst
  0MB ./Selections.rst
  0MB ./About.rst
  0MB ./brc.pyc
  0MB ./Test style.rst
  0MB ./Search.html

.. code:: bash

  echo "Output to stdout"
  echo "Output to stderr" >&2

::

  Output to stdout
  Output to stderr

Fusce rutrum auctor urna, eget consectetur ligula posuere nec. Vivamus sed turpis quis nisi mollis porta. Sed mi magna, ultrices.


=====
Lists
=====

One level
=========

* Pellentesque elementum massa a velit posuere quis eleifend libero ornare.
* Suspendisse quis purus nec turpis dictum laoreet.
* Praesent at erat sit amet metus aliquet aliquam.

#. Pellentesque vel velit eget tellus laoreet bibendum quis ut nisi.
#. Etiam tincidunt leo sed quam pharetra nec porta nisi egestas.
#. Proin pulvinar nunc lobortis lectus bibendum ut faucibus nunc placerat.

Nested
======

* Donec convallis lorem nec leo rhoncus non pharetra tortor auctor.

  * Donec pellentesque lorem id risus porttitor viverra eu eu risus.

* Curabitur a quam sed justo commodo laoreet.

  Morbi pellentesque congue sapien, sed cursus mauris luctus ac. Vestibulum enim mauris, facilisis tincidunt volutpat quis, scelerisque vitae nisl. Sed.

  * Duis vitae orci ut justo ornare gravida.
  * Mauris porta imperdiet urna, a hendrerit turpis mollis vel.

* Ut scelerisque nulla tempor mauris dignissim tempus.

  #. In ac leo eu nibh condimentum elementum quis sit amet dolor.

    Aliquam vestibulum orci felis. Nam non erat non nulla varius tincidunt. Phasellus et commodo justo. Vivamus interdum, eros eu tincidunt.

  #. Donec condimentum elit ut metus commodo condimentum.
  #. Donec eget mi justo, ut ultricies nunc.

* Nam vitae neque neque, id scelerisque metus.
* Donec suscipit tortor eu nunc feugiat non volutpat ligula convallis.

Definition list
===============

Nam felis magna
  Placerat nec consectetur eleifend, accumsan in est.

  Praesent lorem velit, consectetur ullamcorper porta sit amet, sodales commodo.

Nam sit amet tellus nibh.
  Mauris tristique, odio ut venenatis suscipit, sem est auctor lacus, non congue nulla mi id.


======
Tables
======

One table
=========

+--------+--------+
| Field1 | Field2 |
+========+========+
| Cell 1 | Cell 2 |
+--------+--------+

Two tables
==========

+--------+--------+
| Field1 | Field2 |
+========+========+
| Cell 1 | Cell 2 |
+--------+--------+

+--------+--------+
| Field1 | Field2 |
+========+========+
| Cell 1 | Cell 2 |
+--------+--------+

Surrounded
==========

Morbi mollis ullamcorper nisl, id vestibulum nisi sodales a. Nam velit diam, pulvinar et commodo nec, dictum eget odio. Pellentesque.

+--------+--------+
| Field1 | Field2 |
+========+========+
| Cell 1 | Cell 2 |
+--------+--------+

Fusce placerat mi ac ipsum congue in semper diam cursus. Aenean cursus mi id nulla dapibus gravida. Nulla diam ante.


==========
Hyperlinks
==========

This is a link to Google_ and this is a link to Tables_ section.

.. _Google: http://google.com


========
Sections
========

h3
==

Integer a mauris eu ipsum molestie mattis. Nunc commodo nunc condimentum lorem mollis in semper nulla tincidunt. Aliquam viverra convallis.

h4
--

In quis diam ac diam consectetur cursus ac ac justo. Curabitur ut tellus nibh, at vehicula ante. Sed vulputate sem.

h5
^^

Fusce blandit euismod elit, id fermentum risus mattis vel. Sed arcu turpis, accumsan eget iaculis sit amet, condimentum sed dui.

h6
""

Quisque sit amet dui arcu, vitae fermentum leo. Nulla nec augue eget diam malesuada rhoncus egestas quis quam. Etiam aliquet.


======
Images
======

One image
=========

.. figure:: https://lh6.googleusercontent.com/-JFqmfxB_MLo/SNAbesnDDvI/AAAAAAAABNY/MoOrV0CQWbc/s800/Clip%2520smile-200x200.jpg
  :target: http://yjl.im

  Clip smile

  Vivamus vel tincidunt diam. Mauris accumsan, nisi congue egestas lacinia, tellus nulla consequat dolor, a egestas nisl dolor vitae erat.

Images
======

.. figure:: https://lh6.googleusercontent.com/-JFqmfxB_MLo/SNAbesnDDvI/AAAAAAAABNY/MoOrV0CQWbc/s800/Clip%2520smile-200x200.jpg
.. figure:: https://lh6.googleusercontent.com/-JFqmfxB_MLo/SNAbesnDDvI/AAAAAAAABNY/MoOrV0CQWbc/s800/Clip%2520smile-200x200.jpg

  Clip smile

.. figure:: https://lh6.googleusercontent.com/-JFqmfxB_MLo/SNAbesnDDvI/AAAAAAAABNY/MoOrV0CQWbc/s800/Clip%2520smile-200x200.jpg

  Clip smile

  Vivamus vel tincidunt diam. Mauris accumsan, nisi congue egestas lacinia, tellus nulla consequat dolor, a egestas nisl dolor vitae erat.

Proin ullamcorper, quam eget tincidunt adipiscing, est diam accumsan dolor, in dapibus eros urna ac erat. Etiam bibendum feugiat ante.

Alignments
==========

Curabitur sagittis, odio vel adipiscing hendrerit, nisl mauris vulputate felis, vel aliquam velit odio in dolor. Nullam laoreet lobortis consectetur. Nam bibendum pharetra velit, vitae tincidunt sem fermentum quis. In et massa eros. Fusce vitae massa vel elit viverra gravida. Aliquam a mi urna, in pulvinar turpis. Aenean erat sapien, tempor sed venenatis et, tempus non ante.

.. figure:: https://lh6.googleusercontent.com/-JFqmfxB_MLo/SNAbesnDDvI/AAAAAAAABNY/MoOrV0CQWbc/s800/Clip%2520smile-200x200.jpg
  :align: right

In ultricies odio vel ipsum ullamcorper rutrum. Mauris ac massa ligula. Cras non leo luctus augue viverra viverra ac ut massa. Maecenas fringilla mauris at nunc euismod ultrices. Vestibulum eu diam fermentum mauris dignissim condimentum. Fusce ante mi, sagittis sit amet pulvinar et, vehicula ut quam. Proin in lorem neque. Sed lacinia placerat augue, id interdum elit adipiscing in. Pellentesque justo libero, scelerisque quis bibendum eu, scelerisque id turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed enim urna, iaculis non tempus et, vestibulum non sem.

.. figure:: https://lh6.googleusercontent.com/-JFqmfxB_MLo/SNAbesnDDvI/AAAAAAAABNY/MoOrV0CQWbc/s800/Clip%2520smile-200x200.jpg
  :align: left

Nulla facilisi. Donec elit urna, ultricies eget placerat ut, vestibulum eget purus. Cras viverra neque sit amet massa aliquet quis aliquet nisi egestas. In dignissim nunc feugiat lorem facilisis et rutrum dui rhoncus. Vivamus eros lorem, adipiscing vel fermentum dapibus, porta id ipsum. Phasellus bibendum ultrices sollicitudin. Fusce varius, sapien et euismod ultricies, nisl diam suscipit dui, vel dignissim velit quam eleifend diam. Nam eu iaculis arcu. Ut convallis ipsum in felis aliquet rhoncus quis vel est. Vivamus libero sapien, molestie eget convallis ut, consequat vel.


=========
Footnotes
=========

One footnote
============

Etiam a turpis justo [#1]_, a dignissim dolor. Integer risus nulla, venenatis id congue ac, gravida molestie diam. Donec nunc elit.

.. [#1] Praesent est risus, pretium et sagittis nec, placerat vel libero. Duis in lectus quis magna vehicula tincidunt pulvinar a augue.

Two footnotes
=============

Curabitur ligula metus [#2]_, sollicitudin sed aliquet ut, congue in risus. Proin [#3]_ diam risus, dignissim at porttitor vel, tempus at risus.

.. [#2] Phasellus ornare metus et metus rutrum tempus. Aliquam vel hendrerit enim. Praesent rutrum turpis et quam ultricies at sollicitudin ligula.
.. [#3] Aenean in nisi vitae diam tincidunt tristique in id nisi. Quisque sit amet porttitor enim. In hac habitasse platea dictumst.

   Maecenas libero leo, tempus in elementum ac, dignissim vel odio. Aenean sit amet mauris ac sem cursus hendrerit. Sed commodo.


=========
Citations
=========

In a nulla non mi mattis [cite]_ elementum quis in quam. Fusce lobortis malesuada leo, vitae tempus risus venenatis a. Vivamus.

.. [cite] Duis tristique lectus placerat velit ullamcorper sed porttitor libero lacinia. Vivamus semper semper pellentesque. Pellentesque habitant morbi tristique senectus et.

   Ut at posuere nulla. Proin pharetra, nibh a pulvinar dignissim, magna neque aliquam mi, at blandit velit erat nec velit.


===========
Admonitions
===========

.. note:: Fusce quis dui risus. Pellentesque quam ligula, ultrices nec dignissim in, condimentum eu est. Ut lobortis bibendum lacus a suscipit.
.. hint:: Cras eu risus ut felis pretium pretium. Duis ligula velit, vestibulum at pretium in, fringilla non odio. Nullam ultricies pulvinar.
.. warning:: Donec tincidunt magna et nisl fringilla a sagittis quam facilisis. Vestibulum posuere blandit est, in tempor eros dictum at. Integer.
.. tip:: Fusce porttitor vulputate convallis. Vivamus ultricies adipiscing velit. Pellentesque odio turpis, interdum non accumsan sed, aliquam quis neque. Cras tempus.


======
Embeds
======

Gists
=====

Single Gist
-----------

.. raw:: html

  <script src="https://gist.github.com/livibetter/4035958.js?file=send-to-picasa.sh"></script>

Entire Gist
-----------

.. raw:: html

  <script src="https://gist.github.com/livibetter/4035958.js"></script>
