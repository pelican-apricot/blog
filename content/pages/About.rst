:title: About
:author: Yu-Jie Lin
:date: 2016-02-24T06:17:44Z

This is the blog for Pelican Apricot_ theme, and it will have blog postings
regarding the development of the theme and contents to help demonstrate the
features of the theme. Most likely, they would be the experience I gain from
making this theme and perhaps some notes.

.. _Apricot: https://bitbucket.org/pelican-apricot/theme

I won't put any documentation here since this is supposed to be a blog. For
other information as well as issues or feature requests, please visit the
official repository on Bitbucket.
